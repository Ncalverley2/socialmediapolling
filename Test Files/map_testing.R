#load libraries
source(file = "R/load.R")

## Load data
load("sampleTweets.RData")

tweetDataFrame$x <- tweetDataFrame$latitude
tweetDataFrame$y <- tweetDataFrame$longitude
tweetDataFrame$record_key <- seq(1:nrow(tweetDataFrame))

temp <- subset(tweetDataFrame, select=c("record_key", "x", "y"))
row.names(temp) <- temp$record_key; temp$record_key <- NULL

temp$region <- convertLatLongState(temp)

#get wif file
wip <- read.csv("wip.csv") 

#get map data for US counties and states
county_map <- map_data("county")
state_map <- map_data("state")

#merge wip and county_map
wip_map <- merge(county_map, wip, by.x=c("region", "subregion"), 
                 by.y=c("region","subregion"), all.x=TRUE)

#resort merged data
wip_map <- arrange(wip_map, group, order)

#relpace NA with 0's
wip_map[is.na(wip_map)] <- 0

#generate a disctrete color pallette    
pal <- c("#F7FCF5","#74C476","#41AB5D","#238B45","#006D2C","#00441B")


theme_clean <- function(base_size = 12) {
  require(grid)
  theme_grey(base_size) %+replace%
    theme(
      axis.title      =   element_blank(),
      axis.text       =   element_blank(),
      panel.background    =   element_blank(),
      panel.grid      =   element_blank(),
      axis.ticks.length   =   unit(0,"cm"),
      axis.ticks.margin   =   unit(0,"cm"),
      panel.margin    =   unit(0,"lines"),
      plot.margin     =   unit(c(0,0,0,0),"lines"),
      complete = TRUE
    )
}



ggplot( wip_map, aes( x = long , y = lat , group=group ) ) +
  geom_polygon( colour = "grey" , aes( fill = factor( CATEGORY ) ) ) +
  scale_fill_manual( values = pal ) +
  expand_limits( x = wip_map$long, y = wip_map$lat ) +
  coord_map( "polyconic" ) + 
  labs(fill="Number Per\nCounty") + 
  theme_clean( ) +
  geom_path( data = state_map , colour = "red")









